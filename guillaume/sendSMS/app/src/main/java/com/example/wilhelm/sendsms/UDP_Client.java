package com.example.wilhelm.sendsms;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * Created by wilhelm on 13/05/16.
 */
public class UDP_Client {
    private final ThreadLocal<AsyncTask<Void, Void, Void>> async_cient = new ThreadLocal<>();
    public String Message;
    public String ipAddr;

    @SuppressLint("NewApi")
    public void NachrichtSenden() {
        async_cient.set(new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    DatagramChannel d = DatagramChannel.open();
                    d.socket().setBroadcast(true);
                    d.socket().bind(new InetSocketAddress(0));
                    d.connect(new InetSocketAddress(ipAddr, 51117));
                    ByteBuffer b = ByteBuffer.allocate(10000);
                    byte[] s = Message.getBytes("UTF-8");
                    System.out.println(s.length);
                    for(byte c: s){
                        b.put(c);
                    }
                    b.flip();
                    d.write(b);
                    b.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
            }
        });

        if (Build.VERSION.SDK_INT >= 11)
            async_cient.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else async_cient.get().execute();
    }

    public UDP_Client(String ipAddr) {
        this.ipAddr = ipAddr;
    }
}