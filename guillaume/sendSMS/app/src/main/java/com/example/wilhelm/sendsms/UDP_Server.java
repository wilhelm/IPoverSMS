package com.example.wilhelm.sendsms;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;

/**
 * Created by wilhelm on 13/05/16.
 */
public class UDP_Server
{
    private AsyncTask<Void, Void, Void> async;
    private boolean Server_aktiv = true;
    private String nbPhone;

    @SuppressLint("NewApi")
    public void runUdpServer()
    {
        async = new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... params)
            {
                DatagramSocket serverSocket = null;
                try {
                    serverSocket = new DatagramSocket(51117);
                }
                catch(SocketException e) {
                    e.printStackTrace();
                }
                byte[] receiveData = new byte[5000];
                byte[] sendData = new byte[5000];
                while(true) {
                    try {
                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                        serverSocket.receive(receivePacket);
                        String sentence = new String(receivePacket.getData(), Charset.defaultCharset());
                        sentence = sentence.split("~")[0];//TODO : deal with garbage
                        //System.out.println("Message reçu : " + sentence);
                        try {
                            SmsManager smsManager = SmsManager.getDefault();
                            ArrayList<String> parts = smsManager.divideMessage(sentence);
                            smsManager.sendMultipartTextMessage(nbPhone, null, parts, null, null);
                            System.out.println("Destinataire : " + nbPhone + ", message : " + sentence);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= 11) async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else async.execute();
    }

    public void stop_UDP_Server()
    {
        Server_aktiv = false;
    }

    public UDP_Server(String nbPhone) {
        this.nbPhone = nbPhone;
    }
}
