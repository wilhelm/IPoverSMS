package com.example.wilhelm.sendsms;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Hashtable;

public class Client {
	public static void main(String[] args) throws IOException {
		DatagramChannel d = DatagramChannel.open();
		d.socket().setBroadcast(true);
		d.socket().bind(new InetSocketAddress(0));
		d.connect(new InetSocketAddress("10.8.0.1", 1456));
		DatagramChannel d1 = DatagramChannel.open();
		//while(true) 
		sendMessage("src",d);
		ByteBuffer dst = ByteBuffer.allocate(4096);
		Hashtable<String, String> digests = new Hashtable<String, String>();
		while(true) {
			/* get name of the file */
			d.receive(dst);
			dst.flip();
			CharBuffer c = Charset.defaultCharset().decode(dst);
			String s = "";
			while(c.hasRemaining()) {
				s += c.get();
			}
			//System.out.println("digest : "+s);
			dst.clear();
			/* get file digest */
			d.receive(dst);
			dst.flip();
			CharBuffer c1 = Charset.defaultCharset().decode(dst);
			String s1 = "";
			while(c1.hasRemaining()) {
				s1 += c1.get();
			}
			System.out.println(s1);
			dst.clear();
			if(digests.put(s, s1) != s1) { //if we get a new digest for this file
				//ask for file
				sendMessage(s,d);
				d.receive(dst);
				dst.flip();
				CharBuffer c2 = Charset.defaultCharset().decode(dst);
				String s2 = "";
				while(c2.hasRemaining()) {
					s2 += c2.get();
				}
				System.out.println(s2);
				dst.clear();
			}
			//sendMessage(s,d1);
			dst.clear();
		}
		/*d.receive(dst);
		dst.flip();
		
		System.out.println(s);
		//udpServer();*/
	}
	public static void sendMessage(String s, DatagramChannel d) throws IOException {
		ByteBuffer b = StandardCharsets.UTF_8.encode(s);
		d.write(b);
		/*ByteBuffer b = ByteBuffer.allocate(1024);
		for(char c: s.toCharArray()){
			b.putChar(c);
			System.out.println(c);
		}
		b.flip();
		d.write(b);*/
	}
}
