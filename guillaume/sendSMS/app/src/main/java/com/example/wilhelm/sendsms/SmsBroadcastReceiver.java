package com.example.wilhelm.sendsms;

/**
 * Created by wilhelm on 13/05/16.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";

    String IP = "10.42.0.1";
    DatagramChannel d;
    ByteBuffer b;

    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        MainActivity inst2 = MainActivity.instance();
        //System.out.println(inst2);
        UDP_Client Client = new UDP_Client(inst2.ipAddr);

        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            for (int i = 0; i < sms.length; ++i) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                String smsBody = smsMessage.getMessageBody().toString();
                String address = smsMessage.getOriginatingAddress();

                //smsMessageStr += "SMS From: " + address + "\n";
                smsMessageStr += smsBody;// + "\n";
            }
            Toast.makeText(context, smsMessageStr, Toast.LENGTH_SHORT).show();

            //this will update the UI with message
            MainActivity inst = MainActivity.instance();
            Log.i("SMS", "Message arrived");
            Client.Message = smsMessageStr;
            //System.out.println(smsMessageStr);
            //Send message
            Client.NachrichtSenden();
            /*try {
                sendMessage(smsMessageStr);
            }
            catch (IOException e) {
                Log.e("Paquet", "Paquet non envoyé");
            }*/
            //inst.updateList(smsMessageStr);
        }
    }

    public void sendMessage(String s) throws IOException {
        for(char c: s.toCharArray()){
            b.putChar(c);
            System.out.print(c);
        }
        System.out.println();
        b.flip();
        d.write(b);
        b.clear();
    }
}