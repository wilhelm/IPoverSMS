package com.example.wilhelm.sendsms;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class MainActivity extends Activity implements OnItemClickListener {

    private static MainActivity inst;
    ArrayList<String> smsMessagesList = new ArrayList<String>();
    ListView smsListView;
    ArrayAdapter arrayAdapter;
    public String ipAddr;

    public static MainActivity instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            System.out.println("appli lancée");
            smsManager.sendTextMessage("0688715561", null, "appli lancée", null, null);
            System.out.println("envoyé");
        }

        catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_sms);
        /*smsListView = (ListView) findViewById(R.id.SMSList);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, smsMessagesList);
        smsListView.setAdapter(arrayAdapter);
        smsListView.setOnItemClickListener(this);*/
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ipAddr = ((EditText) findViewById(R.id.editText)).getText().toString();
                String nbPhone = ((EditText) findViewById(R.id.editText2)).getText().toString();
                UDP_Server Server = new UDP_Server(nbPhone);
                Server.runUdpServer();
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    System.out.println("serveur udp lancé");
                    smsManager.sendTextMessage(nbPhone, null, "serveur udp lancé", null, null);
                    System.out.println("envoyé");
                }

                catch (Exception e) {
                    e.printStackTrace();
                }
                refreshSmsInbox();
            }
        });
    }

    public void refreshSmsInbox() {
        ContentResolver contentResolver = getContentResolver();
        Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
        int indexBody = smsInboxCursor.getColumnIndex("body");
        int indexAddress = smsInboxCursor.getColumnIndex("address");
    }

    public void updateList(final String smsMessage) {
        arrayAdapter.insert(smsMessage, 0);
        arrayAdapter.notifyDataSetChanged();
    }

    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        try {
            String[] smsMessages = smsMessagesList.get(pos).split("\n");
            String address = smsMessages[0];
            String smsMessage = "";
            for (int i = 1; i < smsMessages.length; ++i) {
                smsMessage += smsMessages[i];
            }

            String smsMessageStr = address + "\n";
            smsMessageStr += smsMessage;
            Toast.makeText(this, smsMessageStr, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
