#!/usr/bin/python3
from base import *
tableau = ['@','Δ',' ','0','¡','P','¿','p','£','_','!','1','A','Q','a','q','$','Φ','"','2','B','R','b','r','¥','Γ','#','3','C','S','c','s','è','Λ','¤','4','D','T','d','t','é','Ω','%','5','E','U','e','u','ù','Π','&','6','F','V','f','v','ì','Ψ','\'','7','G','W','g','w','ò','Σ','(','8','H','X','h','x','Ç','Θ',')','9','I','Y','i','y','\n','Ξ','*',':','J','Z','j','z','Ø','+',';','K','Ä','k','ä','ø','Æ',',','<','L','Ö','l','ö','\r','æ','-','=','M','Ñ','m','ñ','Å','ß','.','>','N','Ü','n','ü','å','É','/','?','O','§','o','à']

def convertIntToGSM(i):
    return tableau[i]

def convertGSMtoInt(char):
    for k in range(127):
        if (tableau[k] == char):
            return k

def gsmencode(B):
    return "".join(map(convertIntToGSM, (list(basetoN(len(B), 127)) + [0, 0])[:2] +  list(basetoN(frombytes(B), 127))))

def gsmdecode(B):
    return bytes(tobytes(basefromN(map(convertGSMtoInt, B[2:]), 127), basefromN(map(convertGSMtoInt,B[:2]), 127)))

if __name__=="__main__":
    msg = b"coucou ! j'aime le monde !"
    print(gsmencode(msg))
    print(gsmdecode(gsmencode(msg)))

