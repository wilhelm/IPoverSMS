def basetoN(num,base):
    """Converts an integer to base <base>, most significant digit last, without trailing 0"""
    current=num
    while current!=0:
        current, remainder=divmod(current, base)
        yield remainder

def basefromN(repr, base):
    """Converts the iterable given by basetoN(n, base) back to n."""
    n = 0
    puissance = 1
    for chiffre in repr:
        n+=chiffre*puissance
        puissance*=base
    return n

def frombytes(B):
    """Converts a sequence of bytes to the number it represents (most significant byte last)."""
    n = 0
    for i, b in enumerate(B):
        n|=b<<(8*i)
    return n

def tobytes(n, length):
    """Converts a number to a sequence of bytes (most significant bit last), padding with 0 as necessary to go up to specified length."""
    for i in range(length):
        yield (n&(0xff<<(8*i)))>>(8*i)

if __name__=="__main__":
    print(list(basetoN(12, 2)))
    print(basefromN(basetoN(12, 2), 2))
    print(frombytes(b"abcdefg"))
    print(bytes(tobytes(frombytes(b"abcdefg"), 7)))
