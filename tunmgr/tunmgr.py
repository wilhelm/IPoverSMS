#!/usr/bin/python3

from pytun import TunTapDevice
import selectors
import socket
import sys
import traceback
from math import log

from gsm import *

SMS_SIZE = 153*14 # lettres GSM
PEER = sys.argv[1]
PORT = 51117

tun = TunTapDevice()

addrs = ("10.8.0.1", "10.8.0.2")
if len(sys.argv)>2:
    addrs = tuple(reversed(addrs))
tun.addr, tun.dstaddr = addrs
tun.netmask = '255.255.255.0'
tun.mtu = int((SMS_SIZE - 2)*log(127)/log(256)) - 4 # header tun et header gsmencode
print(tun.mtu)

tun.up()

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('', PORT))


sel = selectors.DefaultSelector()

sel.register(tun, selectors.EVENT_READ, "tun")
sel.register(s, selectors.EVENT_READ, "sms")

while 1:
    for key, events in sel.select():
        try:
            if key.data=="tun":
                msg = tun.read(tun.mtu + 4)
                print("tun", msg, len(msg), "bytes")
                msg = (gsmencode(msg) + "~").encode("utf8")
                print("gsmencode", msg, len(msg), "bytes")
                s.sendto(msg, (PEER, PORT))
            elif key.data == "sms":
                msg = s.recv((tun.mtu+4)*4)
                print("sms", msg, len(msg), "bytes")
                msg = gsmdecode(msg.decode("utf8"))
                print("sms decodé", msg, len(msg), "bytes")
                tun.write(msg)
        except Exception:
            traceback.print_exc()
            



